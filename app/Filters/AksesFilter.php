<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class AksesFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $statusCode = 401;
		try
		{
            $key = getenv('JWT_SECRET_KEY');
            $header = $request->getHeaderLine("Authorization");
            $token = str_replace('Bearer ', '', $header);

            if(!$token){
                throw new \Exception('Belum login');
            }

            $decoded = JWT::decode($token, new Key($key, 'HS256'));
            $decoded_array = (array) $decoded;
            if(is_array($arguments)){
                $grupUserLogin = $decoded_array['grup'];
                $lolos = false;
                foreach($arguments as $argument){
                    if(in_array($argument, $grupUserLogin)){
                        $lolos = true;
                        break;
                    }
                }
                if(!$lolos){
                    $statusCode = 403;
                    throw new \Exception('Hak akses ditolak');
                }
            }
            $request->setGlobal('akses_data', $decoded_array);
		}
		catch (\Exception $e)
		{
            $response = service('response');
            return $response->setHeader('Access-Control-Allow-Origin', getenv('CORS_ALLOW_ORIGIN'))
            ->setHeader('Access-Control-Allow-Headers', getenv('CORS_ALLOW_HEADERS'))
            ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
            ->setStatusCode($statusCode)->setJSON([
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}