<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class CorsFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
		header('Access-Control-Allow-Origin: '.getenv('CORS_ALLOW_ORIGIN'));
        header('Access-Control-Allow-Headers: '.getenv('CORS_ALLOW_HEADERS'));
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        if(!$request->isCLI()){
            $method = $_SERVER['REQUEST_METHOD'];
            if ($method == "OPTIONS") {
                die();
            }
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}