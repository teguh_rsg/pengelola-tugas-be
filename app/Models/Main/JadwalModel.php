<?php

namespace App\Models\Main;

use CodeIgniter\Model;

class JadwalModel extends Model
{
    protected $table = 'jadwal';
    protected $primaryKey = 'id';
    protected $useSoftDeletes = false;
    protected $returnType = 'object';

    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = '';

    protected $allowedFields = ['semester', 'dosen', 'hari', 'mulai', 'selesai', 'mata_kuliah', 'pjm', 'created_by'];
}
