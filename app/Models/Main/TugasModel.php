<?php

namespace App\Models\Main;

use CodeIgniter\Model;

class TugasModel extends Model
{
    protected $table = 'tugas';
    protected $primaryKey = 'id';
    protected $useSoftDeletes = false;
    protected $returnType = 'object';

    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = '';

    protected $allowedFields = ['jadwal_id', 'judul', 'detail', 'deadline', 'jenis', 'created_by'];
}
