<?php

namespace App\Models\Main;

use CodeIgniter\Model;

class UserGrupModel extends Model
{
    protected $table = 'user_grup';
    protected $primaryKey = 'id';
    protected $returnType = 'object';

    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = '';

    protected $allowedFields = ['user_id', 'grup_id'];

    public function codeByUser($id){
        $builder = $this->db->table('user_grup');
        $builder->select('grup.code');
        $builder->join('grup', 'grup.id = user_grup.grup_id');
        $builder->where('user_grup.user_id', $id);
        $query = $builder->get();
        $code = [];
        foreach($query->getResult() as $row){
            $code[] = $row->code;
        }
        return $code;
    }
}
