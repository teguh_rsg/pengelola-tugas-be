<?php

namespace App\Models\Main;

use CodeIgniter\Model;

class KonfigurasiModel extends Model
{
    protected $table = 'konfigurasi';
    protected $primaryKey = 'id';
    protected $useSoftDeletes = true;
    protected $returnType = 'object';

    protected $useTimestamps = true;

    protected $allowedFields = ['nama', 'nilai', 'kategori'];
}
