<?php

namespace App\Models\Main;

use CodeIgniter\Model;

class GrupModel extends Model
{
    protected $table = 'grup';
    protected $primaryKey = 'id';
    protected $useSoftDeletes = true;
    protected $returnType = 'object';

    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = '';

    protected $allowedFields = ['code', 'name'];
}
