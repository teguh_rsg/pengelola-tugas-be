<?php

namespace App\Libraries;

class TegTable
{
    private $builder;
    private $dataReq;

    public function __construct($builder, $dataReq){
        $this->builder = $builder;
        $this->dataReq = $dataReq;
    }

    public function searchable($search = []){
        $maxIndex = count($search) - 1;
        if(isset($this->dataReq['search'])){
            foreach($search as $i => $col){
                if($i === 0){
                    $this->builder->groupStart();
                    $this->builder->Like($col, $this->dataReq['search']);
                }else{
                    $this->builder->orLike($col, $this->dataReq['search']);
                }
                if($i === ($maxIndex)){
                    $this->builder->groupEnd();
                }
            }
        }
        return $this;
    }

    public function result(){
        $limit = $this->dataReq['length'] ?? 10;
        $offset = $this->dataReq['start'] ?? 0;
        $this->builder->limit($limit, $offset);

        $order = $this->dataReq['order'] ?? [];
        $column = $order['column'] ?? null;
        $dir = $order['dir'] ?? 'ASC';
        if(isset($column)){
            $this->builder->orderBy($column, $dir);
        }
        
        $res['total'] = $this->builder->countAllResults(false);
        $res['data'] = $this->builder->get()->getResultArray();
        return $res;
    }

}
