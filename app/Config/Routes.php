<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// $routes->get('/', 'Home::index');

$routes->options('(:any)', 'Options::index');

$routes->post('login', 'Auth::login'); //login

$routes->group('admin', ['filter' => 'aksesFilter:root,admin'], static function ($routes) {
    $routes->resource('user', ['controller' => 'Admin\User']);
    $routes->resource('grup', ['controller' => 'Admin\Grup']);
    $routes->resource('user-grup', [
        'controller' => 'Admin\UserGrup',
        'only' => ['create', 'delete'],
    ]);
    $routes->get('grup-by-user/(:num)', 'Admin\UserGrup::show/$1');
});

$routes->group('general', ['filter' => 'aksesFilter'], static function ($routes) {
    $routes->post('change-password', [\App\Controllers\General\UserSetting::class, 'changePassword']);

    $routes->get('ambil-feed', [\App\Controllers\Main\Pengumuman::class, 'ambilFeed']);
});

$routes->group('mahasiswa', ['filter' => 'aksesFilter'], static function ($routes) {
    $routes->resource('jadwal', ['controller' => 'Main\Jadwal']);
    $routes->resource('tugas', ['controller' => 'Main\Tugas']);
});


$routes->group('list-option', ['filter' => 'aksesFilter'], static function ($routes) {
    $routes->get('grup', [\App\Controllers\Admin\Grup::class, 'optionGrup']);
});

// php index.php command cron notifikasi
$routes->cli('command/cron/notifikasi', 'Command\Cron::notifikasi');
