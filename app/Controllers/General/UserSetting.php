<?php

namespace App\Controllers\General;

use CodeIgniter\RESTful\ResourceController;

class UserSetting extends ResourceController
{
    protected $modelName = 'App\Models\Main\UserModel';
    protected $format    = 'json';

	public function changePassword()
	{
        $validation = \Config\Services::validation();
        $validation->setRules([
            'password' => 'required',
            'password_new' => 'required|max_length[255]|min_length[2]',
            'password_repeat'  => 'required|matches[password_new]',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
		
        $validData = $validation->getValidated();
		$id = $this->request->fetchGlobal('akses_data', 'id');
		$user = $this->model->where("id", $id)->first();
		if(!password_verify($validData['password'], $user->password)){
			return $this->respond(['message' => 'Password salah'], 400);
		}

		$passwordNew = password_hash($validData['password_new'], PASSWORD_DEFAULT);
        if($this->model->where('id', $id)->set([
			'password' => $passwordNew,
		])->update()){
            return $this->respond([
                'message' => 'Berhasil update password',
                'id'=>$id
            ]);
        }
        return $this->respond(['message' => 'Gagal update password'], 400);
	}
}