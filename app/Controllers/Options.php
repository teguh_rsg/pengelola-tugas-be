<?php

namespace App\Controllers;

class Options extends BaseController
{
	public function index()
    {
        return $this->response->setHeader('Access-Control-Allow-Origin', getenv('CORS_ALLOW_ORIGIN'))
            ->setHeader('Access-Control-Allow-Headers', getenv('CORS_ALLOW_HEADERS'))
            ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
            ->setStatusCode(200); //status code
    }

}