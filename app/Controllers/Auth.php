<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

use Firebase\JWT\JWT;

class Auth extends ResourceController
{
	public function login()
	{
		$userModel = new \App\Models\Main\UserModel();

		$email = $this->request->getVar('email');
		$password = $this->request->getVar('password');
		if(!$email || !$password){
			return $this->response->setStatusCode(400)->setJSON([
                'message' => 'Email atau Password Belum Diisi',
            ]);
		}
		
		$user = $userModel->where("email", $email)->first();
		
		if($user && password_verify($password, $user->password)){
			//list grup
			$userGrup = model(\App\Models\Main\UserGrupModel::class);
			//jwt-proses
			$key = getenv('JWT_SECRET_KEY');
			$payload = [
				'username' => $user->username,
				'grup' => $userGrup->codeByUser($user->id),
				'id' => $user->id,
				'nbf' => date("U", strtotime('-1 seconds')),
				'exp' => date("U", strtotime('+1 days')),
			];

			$jwt = JWT::encode($payload, $key, 'HS256');
			//.
			return $this->response->setJSON(['token' => $jwt]);
		}else{
            return $this->response->setStatusCode(400)->setJSON([
                'message' => 'Email atau Password salah',
            ]);
		}
	}

}