<?php

namespace App\Controllers\Main;

use CodeIgniter\RESTful\ResourceController;

class Pengumuman extends ResourceController
{
    protected $format    = 'json';

	public function ambilFeed()
	{   
        $url = getenv('RSS_FEED_PRODI');
        if(!$url){
            return $this->respond([
                'data' => []
            ]);
        }

        $feed = simplexml_load_file($url, "SimpleXMLElement", LIBXML_NOCDATA);
        $data = json_decode(json_encode($feed));
        
        return $this->respond([
            'data' => $data->channel->item
        ]);
	}

}