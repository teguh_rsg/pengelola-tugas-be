<?php

namespace App\Controllers\Main;

use CodeIgniter\RESTful\ResourceController;

class Jadwal extends ResourceController
{
    protected $modelName = 'App\Models\Main\JadwalModel';
    protected $format    = 'json';

	public function index()
	{
        $dataGet = $this->request->getGet();

        $kolom = ['id', 'semester', 'dosen', 'hari', 'mulai', 'selesai', 'mata_kuliah', 'pjm', 'created_at'];
        $builder = $this->model->select($kolom);
        $builder = $this->model->orderBy('semester', 'DESC');
        $builder = $this->model->orderBy('hari', 'ASC');
        $datatable = new \App\Libraries\TegTable($builder, $dataGet);
        $datatable->searchable($kolom);
        return $this->respond($datatable->result());
	}

    public function create(){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'semester' => 'required|greater_than[0]|less_than_equal_to[8]',
            'dosen'  => 'required',
            'hari'  => 'required',
            'mulai'  => 'required',
            'selesai'  => 'required',
            'mata_kuliah'  => 'required',
            'pjm'  => 'permit_empty',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        $validData['created_by'] = $this->request->fetchGlobal('akses_data', 'id');

        if($this->model->insert($validData)){
            return $this->respond([
                'message' => 'Berhasil simpan data',
                'id'=>$this->model->getInsertID()
            ]);
        }
        return $this->respond(['message' => 'Gagal simpan data'], 400);
    }

    public function show($id = null){
        return $this->respond($this->model->select([
            'id', 'semester', 'dosen', 'hari', 'mulai', 'selesai', 'mata_kuliah', 'pjm', 'created_at', 'created_by'
        ])->where('id', $id)->first());
    }

    public function update($id = null){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'semester' => 'required|greater_than[0]|less_than_equal_to[8]',
            'dosen'  => 'required',
            'hari'  => 'required',
            'mulai'  => 'required',
            'selesai'  => 'required',
            'mata_kuliah'  => 'required',
            'pjm'  => 'permit_empty',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        if($this->model->where('id', $id)->set($validData)->update()){
            return $this->respond([
                'message' => 'Berhasil update data',
                'id'=>$id
            ]);
        }
        return $this->respond(['message' => 'Gagal update data'], 400);
    }

    public function delete($id = null){
        if($this->model->delete($id)){
            return $this->respond(['message' => 'Berhasil hapus data']);
        }
        return $this->respond(['message' => 'Gagal hapus data'], 400);
    }
}