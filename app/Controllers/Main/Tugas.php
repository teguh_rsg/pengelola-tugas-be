<?php

namespace App\Controllers\Main;

use CodeIgniter\RESTful\ResourceController;

class Tugas extends ResourceController
{
    protected $modelName = 'App\Models\Main\TugasModel';
    protected $format    = 'json';

	public function index()
	{
        $dataGet = $this->request->getGet();

        $kolom = ['id', 'jadwal_id', 'judul', 'detail', 'deadline', 'jenis', 'created_at'];
        $builder = $this->model->select($kolom);
        if(!empty($dataGet['start_date'])){
            $builder->where('deadline >=', $dataGet['start_date'] . ' 00:00:00');
        }
        if(!empty($dataGet['end_date'])){
            $builder->where('deadline <=', $dataGet['end_date'] . ' 23:59:59');
        }
        $builder = $this->model->orderBy('deadline', 'ASC');
        $datatable = new \App\Libraries\TegTable($builder, $dataGet);
        $datatable->searchable($kolom);
        return $this->respond($datatable->result());
	}

    public function create(){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'jadwal_id' => 'required|is_not_unique[jadwal.id]',
            'judul'  => 'required',
            'deadline'  => 'permit_empty',
            'detail'  => 'required',
            'jenis'  => 'required',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        $validData['created_by'] = $this->request->fetchGlobal('akses_data', 'id');
        if($this->model->insert($validData)){
            return $this->respond([
                'message' => 'Berhasil simpan data',
                'id'=>$this->model->getInsertID()
            ]);
        }
        return $this->respond(['message' => 'Gagal simpan data'], 400);
    }

    public function show($id = null){
        return $this->respond($this->model->select([
            'tugas.id', 
            'tugas.jadwal_id', 
            'tugas.judul', 
            'tugas.detail', 
            'tugas.deadline', 
            'tugas.jenis', 
            'tugas.created_at', 
            'tugas.created_by',
            'jadwal.mata_kuliah AS mata_kuliah'
        ])
        ->join('jadwal', 'tugas.jadwal_id = jadwal.id', 'left')
        ->where('tugas.id', $id)->first());
    }

    public function update($id = null){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'jadwal_id' => 'required|is_not_unique[jadwal.id]',
            'judul'  => 'required',
            'deadline'  => 'permit_empty',
            'detail'  => 'required',
            'jenis'  => 'required',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        if($this->model->where('id', $id)->set($validData)->update()){
            return $this->respond([
                'message' => 'Berhasil update data',
                'id'=>$id
            ]);
        }
        return $this->respond(['message' => 'Gagal update data'], 400);
    }

    public function delete($id = null){
        if($this->model->delete($id)){
            return $this->respond(['message' => 'Berhasil hapus data']);
        }
        return $this->respond(['message' => 'Gagal hapus data'], 400);
    }
}