<?php

namespace App\Controllers\Admin;

use CodeIgniter\RESTful\ResourceController;

class Grup extends ResourceController
{
    protected $modelName = 'App\Models\Main\GrupModel';
    protected $format    = 'json';

	public function index()
	{
        $dataGet = $this->request->getGet();

        $kolom = ['id', 'code', 'name', 'created_at'];
        $builder = $this->model->select($kolom);
        $datatable = new \App\Libraries\TegTable($builder, $dataGet);
        $datatable->searchable($kolom);
        return $this->respond($datatable->result());
	}

    public function create(){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'code' => 'required|max_length[255]|min_length[2]|is_unique[grup.code]',
            'name'  => 'required|max_length[255]|min_length[2]|is_unique[grup.name]',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        if($this->model->insert($validData)){
            return $this->respond([
                'message' => 'Berhasil simpan data',
                'id'=>$this->model->getInsertID()
            ]);
        }
        return $this->respond(['message' => 'Gagal simpan data'], 400);
    }

    public function show($id = null){
        return $this->respond($this->model->select([
            'id', 'code', 'name', 'created_at'
        ])->where('id', $id)->first());
    }

    public function update($id = null){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'code' => "max_length[255]|min_length[2]|is_unique[grup.code,id,{$id}]",
            'name'  => "max_length[255]|min_length[2]|is_unique[grup.name,id,{$id}]",
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        if($this->model->where('id', $id)->set($validData)->update()){
            return $this->respond([
                'message' => 'Berhasil update data',
                'id'=>$id
            ]);
        }
        return $this->respond(['message' => 'Gagal update data'], 400);
    }

    public function delete($id = null){
        $grup = $this->model->where('id', $id)->first();
        if (!$grup) {
            return $this->respond(['message' => 'grup tidak ditemukan'], 404);
        }
        if(getenv('GRUP_CODE_ROOT') === $grup->code){
            return $this->respond(['message' => 'Gagal, GRUP_CODE_ROOT tidak boleh dihapus'], 400);
        }
        if($this->model->delete($id)){
            return $this->respond(['message' => 'Berhasil hapus data']);
        }
        return $this->respond(['message' => 'Gagal hapus data'], 400);
    }

    public function optionGrup(){
        $kolom = ['id', 'code', 'name', 'created_at'];
        $builder = $this->model->select($kolom);
        $builder->where('deleted_at', null);
        return $this->respond([
            'data' => $builder->get()->getResultArray(),
        ]);
    }
}