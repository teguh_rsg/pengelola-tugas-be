<?php

namespace App\Controllers\Admin;

use CodeIgniter\RESTful\ResourceController;

class UserGrup extends ResourceController
{
    protected $modelName = 'App\Models\Main\UserGrupModel';
    protected $format    = 'json';

    public function create(){
        $validation = \Config\Services::validation();
        $validation->setRules([
            'user_id' => 'required|is_natural_no_zero',
            'grup_id'  => 'required|is_natural_no_zero',
        ]);
        if (! $validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        if($this->model->insert($validData)){
            return $this->respond([
                'message' => 'Berhasil simpan data',
                'id'=>$this->model->getInsertID()
            ]);
        }
        return $this->respond(['message' => 'Gagal simpan data'], 400);
    }

    public function show($id = null){
		return $this->respond($this->model->select([
            'user_grup.id id', 'user_grup.user_id user_id', 'user_grup.grup_id grup_id', 'user_grup.created_at created_at',
            'grup.code code', 'grup.name name',
        ])
        ->join('grup', 'user_grup.grup_id = grup.id', 'left')
        ->where('user_grup.user_id', $id)->findAll());
    }

    public function delete($id = null){
        if($this->model->delete($id)){
            return $this->respond(['message' => 'Berhasil hapus data']);
        }
        return $this->respond(['message' => 'Gagal hapus data'], 400);
    }
}