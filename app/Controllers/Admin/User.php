<?php

namespace App\Controllers\Admin;

use CodeIgniter\RESTful\ResourceController;

class User extends ResourceController
{
    protected $modelName = 'App\Models\Main\UserModel';
    protected $format    = 'json';

    public function index()
    {
        $dataGet = $this->request->getGet();

        $kolom = ['id', 'username', 'email', 'created_at'];
        $builder = $this->model->select($kolom);
        $datatable = new \App\Libraries\TegTable($builder, $dataGet);
        $datatable->searchable($kolom);
        return $this->respond($datatable->result());
    }

    public function create()
    {
        $validation = \Config\Services::validation();
        $validation->setRules([
            'username' => 'required|max_length[255]|min_length[3]|is_unique[user.username]',
            'email'  => 'required|max_length[255]|min_length[10]|is_unique[user.email]',
            'password'  => 'required',
        ]);
        if (!$validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        $validData['password'] = password_hash($validData['password'], PASSWORD_DEFAULT);
        if ($this->model->insert($validData)) {
            return $this->respond([
                'message' => 'Berhasil simpan data',
                'id' => $this->model->getInsertID()
            ]);
        }
        return $this->respond(['message' => 'Gagal simpan data'], 400);
    }

    public function show($id = null)
    {
        return $this->respond($this->model->select([
            'id', 'username', 'email', 'created_at'
        ])->where('id', $id)->first());
    }

    public function update($id = null)
    {
        $validation = \Config\Services::validation();
        $validation->setRules([
            'username' => "max_length[255]|min_length[3]|is_unique[user.username,id,{$id}]",
            'email'  => "max_length[255]|min_length[10]|is_unique[user.email,id,{$id}]",
        ]);
        if (!$validation->withRequest($this->request)->run()) {
            $errors = $validation->getErrors();
            return $this->respond(
                [
                    'errors' => $errors,
                    'message' => array_values($errors)[0],
                ],
                400
            );
        }
        $validData = $validation->getValidated();
        if ($this->model->where('id', $id)->set($validData)->update()) {
            return $this->respond([
                'message' => 'Berhasil update data',
                'id' => $id
            ]);
        }
        return $this->respond(['message' => 'Gagal update data'], 400);
    }

    public function delete($id = null)
    {
        if($id === $this->request->fetchGlobal('akses_data', 'id')){
            return $this->respond(['message' => 'Tidak dapat menghapus diri sendiri'], 400);
        }
        $user = $this->model->where('id', $id)->first();
        if (!$user) {
            return $this->respond(['message' => 'User tidak ditemukan'], 404);
        }

        //---user dengan akses tertinggi/GRUP_CODE_ROOT hanya dapat dihapus oleh GRUP_CODE_ROOT
        $grup = $this->request->fetchGlobal('akses_data', 'grup');
        if (!in_array(getenv('GRUP_CODE_ROOT'), $grup)) {
            $userGrup = model(\App\Models\Main\UserGrupModel::class);
            $grupTarget = $userGrup->codeByUser($id);
            if (is_array($grupTarget) && in_array(getenv('GRUP_CODE_ROOT'), $grupTarget)) {
                return $this->respond(['message' => 'Anda tidak memiliki Izin untuk menghapus User dengan Akses Tertinggi'], 400);
            }
        }
        //---end

        if ($this->model->delete($id)) {
            return $this->respond(['message' => 'Berhasil hapus data']);
        }
        return $this->respond(['message' => 'Gagal hapus data'], 400);
    }
}
