<?php

namespace App\Controllers\Command;

use CodeIgniter\Controller;

class Cron extends Controller
{
    public function notifikasi()
    {
        $db = \Config\Database::connect();

        $tugas = $db->table('tugas')
            ->select('tugas.deadline deadline, jadwal.mata_kuliah mata_kuliah')
            ->where("deadline >=",  date('Y-m-d', strtotime('tomorrow')) . ' 00:00:00')
            ->where("deadline <=",  date('Y-m-d', strtotime('tomorrow')) . ' 23:59:59')
            ->join('jadwal', 'tugas.jadwal_id = jadwal.id')
            ->get()
            ->getResult();

        $pesan = 'Besok ada tugas mata kuliah: '.PHP_EOL;
        foreach ($tugas as $row) {
            $pesan .= $row->mata_kuliah .' pukul '.date('H:i', strtotime($row->deadline)) . ', '.PHP_EOL;
        }
        $pesan .= 'yang sudah memasuki deadline.';
        $errTele = "";
        $idBot = getenv('BOT_TELEGRAM');
        $chatGrup = getenv('GROUP_TELEGRAM');
        if ($idBot && $chatGrup && count($tugas) > 0) {
            $curlTele = curl_init();
            $curlUrl = "https://api.telegram.org/bot" . $idBot . "/sendMessage?chat_id=" . $chatGrup . "&text=" . urlencode($pesan);
            curl_setopt_array($curlTele, [
                CURLOPT_URL => $curlUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            ]);

            $response = curl_exec($curlTele);
            $errTele = curl_error($curlTele);

            curl_close($curlTele);
        }
        //        
        if ($errTele) {
            return "cURL Tele Error #:" . $errTele;
        }
        return date('Y-m-d H:i:s') . ' BOT NOTIF TELE GROUP' . PHP_EOL;
    }
}
